# TP2 : Serveur Web

# I. Base de données

>  sur `db.tp2.cesi`.

* installer le paquet `mariadb-server`, puis démarrer le service associé :

```bash=
$ sudo yum install mariadb-server -y
$ sudo systemctl start mariadb.service
$ sudo mysql_secure_installation
```


* se connecter à la base
```bash=
$ sudo mysql -u root -p
Enter password:
```
```bash
Welcome to the MariaDB monitor.  Commands end with ; or \g.
Your MariaDB connection id is 12
Server version: 5.5.68-MariaDB MariaDB Server

Copyright (c) 2000, 2018, Oracle, MariaDB Corporation Ab and others.

Type 'help;' or '\h' for help. Type '\c' to clear the current input statement.
```
* créer une base
```bash=
MariaDB [(none)]> create database db_ben;
MariaDB [(none)]> Query OK, 1 row affected (0.00 sec)

MariaDB [(none)]> show databases;
MariaDB [(none)]> +--------------------+
    -> | Database           |
    -> +--------------------+
    -> | information_schema |
    -> | db_ben             |
    -> | mysql              |
    -> | performance_schema |
    -> +--------------------+
    -> 4 rows in set (0.00 sec)
```
* créer un utilisateur
```bash=
MariaDB [(none)]> create user 'ben_wp'@'%' IDENTIFIED BY 'Pop';
Query OK, 0 rows affected (0.00 sec)
```
* attribuer les droits sur la base de données à l'utilisateur
```bash=
MariaDB [(none)]> GRANT ALL PRIVILEGES ON db_ben.* TO 'ben_wp'@'%';
Query OK, 0 rows affected (0.00 sec)

MariaDB [(none)]> show grants for 'ben_wp'@10.99.99.11
    -> ;
+-----------------------------------------------------------------------------------------------------------------+
| Grants for ben_wp@10.99.99.11                                                                                   |
+-----------------------------------------------------------------------------------------------------------------+
| GRANT USAGE ON *.* TO 'ben_wp'@'10.99.99.11' IDENTIFIED BY PASSWORD '*5AA0BB8F13D89F3DDC473193307FBB5E95623274' |
| GRANT ALL PRIVILEGES ON `db_ben`.* TO 'ben_wp'@'10.99.99.11'                                                    |
+-----------------------------------------------------------------------------------------------------------------+
2 rows in set (0.00 sec)
```
* ouvrir un port firewall pour que d'autres machines puissent accéder à la base
```bash=
$ sudo firewall-cmd --add-port=3306/tcp --permanent
success

$ sudo firewall-cmd --reload
```
* pour déterminer le port utilisé par la base :
```bash=
$ sudo ss -alnpt
State  Recv-Q Send-Q Local Address:Port  Peer Address:Port
LISTEN  0  100  127.0.0.1:25    *:*   users:(("master",pid=1427,fd=13))
LISTEN  0  50  *:3306  *:*  users:(("mysqld",pid=2100,fd=14))
LISTEN  0  128 *:22  *:*  users:(("sshd",pid=1074,fd=3))
LISTEN  0  128 [::]:22  [::]:*  users:(("sshd",pid=1074,fd=4))
```

Pour tester votre base de données :
```bash=
$ curl localhost:3306
5.5.68-MariaDB
zaVk:|%>▒▒J;%HzP]c\!.dmysql_native_password!▒▒#08S01Got packets out of order
```

# II. Serveur Web

> sur `web.tp2.cesi`

Installer un serveur Apache (paquet `httpd` dans CentOS).

Télécharger Wordpress : https://wordpress.org/latest.tar.gz

Idem ici, référez-vous à un des milliers d'article/doc/tuto que vous pourrez trouver. Faites vos recherches en anglais et précisez l'OS qu'on utilise.

Les étapes :
* installer le serveur Web (`httpd`) et le langage PHP (pour installer PHP, voir les instructions plus bas)
```bash=
$ sudo yum install httpd -y

# Installation de dépôt additionels
$ sudo yum install http://rpms.remirepo.net/enterprise/remi-release-7.rpm -y
$ sudo yum install -y yum-utils

# On supprime d'éventuelles vieilles versions de PHP précédemment installées
$ sudo yum remove -y php

# Activation du nouveau dépôt
$ sudo yum-config-manager --enable remi-php56  

# Installation de PHP 5.6.40 et de librairies récurrentes
$ sudo yum install php php-mcrypt php-cli php-gd php-curl php-mysql php-ldap php-zip php-fileinfo -y
```
* télécharger wordpress
```bash=
$ sudo yum install wget -y
$ sudo wget https://wordpress.org/latest.tar.gz
```
* extraire l'archive wordpress dans un dossier qui pourra être servi par Apache
```bash=
$ tar -zxf latest.tar.gz
$ ls
$ latest.tar.gz  wordpress
```
* renseigner à Wordpress les informations qu'il utilisera pour se connecter à la base de données (dans son fichier de configuration)
```bash=
$ sudo cp wp-config-sample.php wp-config.php
$ sudo vim /wordpress/wordpress/wp-config.php
```
```bash
// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'db_ben' );

/** MySQL database username */
define( 'DB_USER', 'ben_wp' );

/** MySQL database password */
define( 'DB_PASSWORD', 'Pop' );

/** MySQL hostname */
define( 'DB_HOST', 'db.tp2.cesi' );
```
* configurer Apache pour qu'il serve le dossier où se trouve Wordpress :
```bash=
$ sudo vim /etc/httpd/conf/httpd.conf
```
```bash
ServerName web.tp2.cesi:80

# Further relax access to the default document root:

DocumentRoot "/usr/etc/wordpress/wordpress"

#
# Relax access to content within /var/www.
#
<Directory "/usr/etc/wordpress">
    AllowOverride None
    # Allow open access:
    Require all granted
</Directory>

# Further relax access to the default document root:
<Directory "/usr/etc/wordpress/wordpress">
</Directory>
```
* on relance le service après sauvegarde du fichier
```bash=
$ sudo systemctl restart httpd
```
* lancer le serveur de base de donnée, puis le serveur web
```bash=
$ sudo systemctl enable httpd
Created symlink from /etc/systemd/system/multi-user.target.wants/httpd.service to /usr/lib/systemd/system/httpd.service.

$ sudo systemctl start httpd.service

$ sudo systemctl status httpd
● httpd.service - The Apache HTTP Server
   Loaded: loaded (/usr/lib/systemd/system/httpd.service; disabled; vendor preset: disabled)
   Active: active (running) since mer. 2021-01-06 15:17:51 CET; 8s ago
     Docs: man:httpd(8)
           man:apachectl(8)
 Main PID: 2207 (httpd)
   Status: "Processing requests..."
   CGroup: /system.slice/httpd.service
           ├─2207 /usr/sbin/httpd -DFOREGROUND
           ├─2208 /usr/sbin/httpd -DFOREGROUND
           ├─2209 /usr/sbin/httpd -DFOREGROUND
           ├─2210 /usr/sbin/httpd -DFOREGROUND
           ├─2211 /usr/sbin/httpd -DFOREGROUND
           └─2212 /usr/sbin/httpd -DFOREGROUND

janv. 06 15:17:51 web.tp2.cesi systemd[1]: Starting The Apache HTTP Server...
janv. 06 15:17:51 web.tp2.cesi httpd[2207]: AH00558: httpd: Could not reliably determine the servers fully qualified domain name, using web.tp2.cesi. Set the ServerName directive globally to suppress this message
janv. 06 15:17:51 web.tp2.cesi systemd[1]: Started The Apache HTTP Server.
```

* ouvrir le port firewall sur le serveur web
```bash=
$ sudo firewall-cmd --add-port=80/tcp --permanent

$ sudo firewall-cmd --reload
success

$ sudo firewall-cmd --list-all
public (active)
  target: default
  icmp-block-inversion: no
  interfaces: enp0s3 enp0s8
  sources:
  services: dhcpv6-client ssh
  ports: 80/tcp
  protocols:
  masquerade: no
  forward-ports:
  source-ports:
  icmp-blocks:
  rich rules:
```
* accéder à l'interface de Wordpress afin de valider la bonne installation de la solution
```bash=
<Page web de setup de Wordpress depuis le navigateur>
```
# III. Reverse Proxy

> A faire uniquement `rp.tp2.cesi`.

Installer un serveur NGINX (paquet `epel-release` puis `nginx` sur CentOS).

Configurer NGINX pour qu'il puisse renvoyer vers le serveur web lorsqu'on l'interroge sur le port 80 :
```bash=
$ sudo yum install epel-release -y
$ sudo yum install nginx -y
```
* on édite le fichier de configuration de Nginx
```bash=
$ sudo vim /etc/nginx/nginx.conf
```
```bash
 server {
        listen       80;
        listen       [::]:80;
        server_name  web.cesi;
        # root         /usr/share/nginx/html;

        # Load configuration files for the default server block.
        #include /etc/nginx/default.d/*.conf;

        location / {
        proxy_pass      http://web.tp2.cesi:80;
        }
```
```bash=
$ sudo systemctl start nginx
$ sudo systemctl enable nginx
Created symlink from /etc/systemd/system/multi-user.target.wants/nginx.service to /usr/lib/systemd/system/nginx.service.
```

* configurer le firewall de NGINX afin d'accepter ce trafic

```bash=
$ sudo firewall-cmd --add-port=80/tcp --permanent
$ sudo firewall-cmd --reload
```
Tester qu'on accède bien au Wordpress en passant par l'IP du reverse proxy. 

# IV. Un peu de sécu

## 1. fail2ban

> A faire sur toutes les machines où on se connecte avec SSH.

fail2ban est un outil très commun au sein des systèmes GNU/Linux, il permet de limiter les attaques à base de flood/spam (comme attaque par bruteforce).

Cas que vous devez mettre en place :

* il lit en temps réel le fichier de log du démon SSH `sshd`
* `sshd` écrit chaque tentative de connexion dans le fichier
* si un certain nombre d'échecs de connexions sont réalisés dans un certain laps de temps, fail2ban devra le détecter
* fail2ban utilisera alors le pare-feu pour bloquer l'IP qui a essayé de se connecter

```bash=
$ sudo yum install epel-release
$ sudo yum install fail2ban fail2ban-systemd
$ sudo sudo systemctl enable fail2ban
```
* créer le fichier jail.local à partir d'un template et éditer notre fichier de "jail" pour SSH :
```bash=
$ sudo cp -pf /etc/fail2ban/jail.conf /etc/fail2ban/jail.local
$ sudo vim /etc/fail2ban/jail.local
```
```bash
[DEFAULT]
bantime  = 30m
findtime  = 10m
maxretry = 3

[sshd]
port    = ssh
logpath = %(sshd_log)s
backend = %(sshd_backend)s
enabled = true
```
* on redémarre le service après avoir sauvegardé le fichier puis on regarde son status
```bash=
$ sudo systemctl restart fail2ban; systemctl status fail2ban
$ sudo fail2ban-client -v status
```
```bash
2021-01-07 12:51:57,472 fail2ban.configreader   [2176]: INFO    Loading configs for fail2ban under /etc/fail2ban
2021-01-07 12:51:57,476 fail2ban.configparserin [2176]: INFO      Loading files: ['/etc/fail2ban/fail2ban.conf']
2021-01-07 12:51:57,478 fail2ban.configparserin [2176]: INFO      Loading files: ['/etc/fail2ban/fail2ban.conf']
2021-01-07 12:51:57,479 fail2ban                [2176]: INFO    Using socket file /var/run/fail2ban/fail2ban.sock
2021-01-07 12:51:57,479 fail2ban                [2176]: INFO    Using pid file /var/run/fail2ban/fail2ban.pid, [INFO] logging to /var/log/fail2ban.log
Status
|- Number of jail:      1
`- Jail list:   sshd
```
* regarder le status de la "jail" SSH (voir tentatives de connexion et IPs bannies)
```bash=
$ sudo fail2ban-client status sshd
```
```bash
Status for the jail: sshd
|- Filter
|  |- Currently failed: 0
|  |- Total failed:     4
|  `- Journal matches:  _SYSTEMD_UNIT=sshd.service + _COMM=sshd
`- Actions
   |- Currently banned: 0
   |- Total banned:     1
   '- Banned IP list:   10.99.99.1
```
* informations du log fail2ban.log
```bash=
$ sudo tail -f /var/log/fail2ban.log
```
```bash
2021-01-07 12:50:57,101 fail2ban.jail           [2161]: INFO    Creating new jail 'sshd'
2021-01-07 12:50:57,149 fail2ban.jail           [2161]: INFO    Jail 'sshd' uses systemd {}
2021-01-07 12:50:57,151 fail2ban.jail           [2161]: INFO    Initiated 'systemd' backend
2021-01-07 12:50:57,158 fail2ban.filter         [2161]: INFO      maxLines: 1
2021-01-07 12:50:57,159 fail2ban.filtersystemd  [2161]: INFO    [sshd] Added journal match for: '_SYSTEMD_UNIT=sshd.service + _COMM=sshd'
2021-01-07 12:50:57,349 fail2ban.filter         [2161]: INFO      maxRetry: 3
2021-01-07 12:50:57,349 fail2ban.filter         [2161]: INFO      encoding: UTF-8
2021-01-07 12:50:57,350 fail2ban.filter         [2161]: INFO      findtime: 600
2021-01-07 12:50:57,350 fail2ban.actions        [2161]: INFO      banTime: 1800
2021-01-07 12:50:57,377 fail2ban.jail           [2161]: INFO    Jail 'sshd' started
2021-01-07 12:54:18,943 fail2ban.filter         [2161]: INFO    [sshd] Found 10.99.99.1 - 2021-01-07 12:54:18
2021-01-07 12:54:22,456 fail2ban.filter         [2161]: INFO    [sshd] Found 10.99.99.1 - 2021-01-07 12:54:22
2021-01-07 12:54:24,883 fail2ban.filter         [2161]: INFO    [sshd] Found 10.99.99.1 - 2021-01-07 12:54:24
2021-01-07 12:54:25,057 fail2ban.actions        [2161]: NOTICE  [sshd] Ban 10.99.99.1
```
* regarder le fichier de logs Syslog pour les tentatives d'intrusion
```bash=
$ sudo cat /var/log/secure | grep 'Failed password'
```
```bash
Jan  7 12:11:33 db sshd[3045]: Failed password for ben from 10.99.99.1 port 49918 ssh2
Jan  7 12:11:39 db sshd[3045]: Failed password for ben from 10.99.99.1 port 49918 ssh2
Jan  7 12:11:42 db sshd[3045]: Failed password for ben from 10.99.99.1 port 49918 ssh2
Jan  7 12:13:51 db sshd[1860]: Failed password for invalid user infeeny from 10.99.99.1 port 49971 ssh2
Jan  7 12:13:54 db sshd[1860]: Failed password for invalid user infeeny from 10.99.99.1 port 49971 ssh2
Jan  7 12:13:57 db sshd[1860]: Failed password for invalid user infeeny from 10.99.99.1 port 49971 ssh2
```
* Débannir une IP bannie
```bash=
$ sudo fail2ban-client -i
$ set sshd unbanip 10.99.99.1
```
## 2. HTTPS

### B. Configurer le reverse proxy

> sur la machine frontale Web : le reverse proxy.

Configurer le reverse proxy pour écouter sur le port 443 (TCP). Le port 443 doit mettre en place un chiffrement TLS avec la clé et le certificat qui viennent d'être générés. Il doit toujours rediriger vers l'application Web Wordpress.

Si le port 80 est atteint, le trafic doit être redirigé automatiquement vers le port 443 (ça se fait dans la configuration de NGINX).
```bash=
$ openssl req -new -newkey rsa:2048 -days 365 -nodes -x509 -keyout web.cesi.key -out web.cesi.crt
```
```bash
Generating a 2048 bit RSA private key
..............................+++
.+++
writing new private key to 'web.cesi.key'
-----
You are about to be asked to enter information that will be incorporated
into your certificate request.
What you are about to enter is what is called a Distinguished Name or a DN.
There are quite a few fields but you can leave some blank
For some fields there will be a default value,
If you enter '.', the field will be left blank.
-----
Country Name (2 letter code) [XX]:FR
State or Province Name (full name) []:Pop
Locality Name (eg, city) [Default City]:Pop City
Organization Name (eg, company) [Default Company Ltd]:Pop Corp
Organizational Unit Name (eg, section) []:Pop
Common Name (eg, your name or your servers hostname) []:web.cesi
```
* récupérer la clé et le certificat > dossier Nginx
```bash=
$ sudo mkdir /etc/pki/nginx
$ sudo cp web.cesi.crt /etc/pki/nginx/web.cesi.crt
$ sudo mkdir /etc/pki/nginx/private
$ sudo cp web.cesi.key /etc/pki/nginx/private/web.cesi.key
```
* rajouter une autorisation parefeu sur le port 443
```bash=
$ sudo firewall-cmd --add-port=443/tcp --permanent
success
$ sudo firewall-cmd --reload
success
$ sudo firewall-cmd --list-ports
```
* éditer le fichier de configuration de Nginx
```bash=
$ sudo vim /etc/nginx/nginx.conf
```
```bash
   server {
        listen       80;
        listen       [::]:80;
        server_name  web.cesi;
        # root         /usr/share/nginx/html;

        # Load configuration files for the default server block.
        include /etc/nginx/default.d/*.conf;

        error_page 404 /404.html;
        location = /404.html {
        }

        error_page 500 502 503 504 /50x.html;
        location = /50x.html {
        }
        return 301 https://$host$request_uri;
    }

# Settings for a TLS enabled server.
#
    server {
        listen       443 ssl http2 default_server;
        listen       [::]:443 ssl http2 default_server;
        server_name  web.cesi;
#        root         /usr/share/nginx/html;

        ssl_certificate "/etc/pki/nginx/web.cesi.crt";
        ssl_certificate_key "/etc/pki/nginx/private/web.cesi.key";
        ssl_session_cache shared:SSL:1m;
        ssl_session_timeout  10m;
        ssl_ciphers HIGH:!aNULL:!MD5;
        ssl_prefer_server_ciphers on;

#        # Load configuration files for the default server block.
        include /etc/nginx/default.d/*.conf;

        location / {
        proxy_pass      http://web.tp2.cesi;
[..]
```
* on redémarre le service Nginx
```bash=
$ sudo systemctl restart nginx
```

## 3. Monitoring

### Présentation

Nous allons dans cette section mettre en place un outil de monitoring et d'alerting simple mais puissant : Netdata. 

Suivez la [doc officielle](https://learn.netdata.cloud/docs/get) pour l'installer. L'interface de l'outil est alors disponible sur le port `19999` du serveur (interface Web).

Netdata a pour avantages de ne consommer que très peu de ressourcesn mais néanmoins remonter une grande quantité de métriques, afin de les exploiter pour du monitoring ou la mise en place d'alertes.

### A. Installation de Netdata

Installer Netdata sur les 3 machines et tester qu'il fonctionne bien en accédant à l'interface Web. Il faudra ouvrir le port firewall `19999/tcp` pour accéder à l'interface de Netdata.

```bash=
bash <(curl -Ss https://my-netdata.io/kickstart.sh)
```