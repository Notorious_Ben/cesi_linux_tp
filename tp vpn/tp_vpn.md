# TP VPN

* état VPN côté serveur : indique une connexion entrante venant de 10.88.88.1
```bash=
$ sudo systemctl status  openvpn@cesi_vpn
```
```bash
● openvpn@cesi_vpn.service - OpenVPN Robust And Highly Flexible Tunneling Application On cesi_vpn
   Loaded: loaded (/usr/lib/systemd/system/openvpn@.service; enabled; vendor preset: disabled)
   Active: active (running) since ven. 2021-01-08 14:22:22 CET; 30min ago
 Main PID: 1068 (openvpn)
   Status: "Initialization Sequence Completed"
   CGroup: /system.slice/system-openvpn.slice/openvpn@cesi_vpn.service
           └─1068 /usr/sbin/openvpn --cd /etc/openvpn/ --config cesi_vpn.conf

janv. 08 14:50:56 vpn.tp2.cesi openvpn[1068]: Fri Jan  8 14:50:56 2021 10.88.88.1:64736 Control Channel: TLSv1.2, cipher TLSv1/SSLv3 ECDHE... bit RSA
janv. 08 14:50:56 vpn.tp2.cesi openvpn[1068]: Fri Jan  8 14:50:56 2021 10.88.88.1:64736 [CESI_CLIENT] Peer Connection Initiated with [AF_I....1:64736
janv. 08 14:50:56 vpn.tp2.cesi openvpn[1068]: Fri Jan  8 14:50:56 2021 CESI_CLIENT/10.88.88.1:64736 MULTI_sva: pool returned IPv4=10.8.0.6...enabled)
janv. 08 14:50:56 vpn.tp2.cesi openvpn[1068]: Fri Jan  8 14:50:56 2021 CESI_CLIENT/10.88.88.1:64736 MULTI: Learn: 10.8.0.6 -> CESI_CLIENT/....1:64736
janv. 08 14:50:56 vpn.tp2.cesi openvpn[1068]: Fri Jan  8 14:50:56 2021 CESI_CLIENT/10.88.88.1:64736 MULTI: primary virtual IP for CESI_CLI...10.8.0.6
janv. 08 14:50:57 vpn.tp2.cesi openvpn[1068]: Fri Jan  8 14:50:57 2021 CESI_CLIENT/10.88.88.1:64736 PUSH: Received control message: 'PUSH_REQUEST'
janv. 08 14:50:57 vpn.tp2.cesi openvpn[1068]: Fri Jan  8 14:50:57 2021 CESI_CLIENT/10.88.88.1:64736 SENT CONTROL [CESI_CLIENT]: 'PUSH_REPLY,redire...
janv. 08 14:50:57 vpn.tp2.cesi openvpn[1068]: Fri Jan  8 14:50:57 2021 CESI_CLIENT/10.88.88.1:64736 Data Channel: using negotiated cipher ...256-GCM'
janv. 08 14:50:57 vpn.tp2.cesi openvpn[1068]: Fri Jan  8 14:50:57 2021 CESI_CLIENT/10.88.88.1:64736 Outgoing Data Channel: Cipher 'AES-256... bit key
janv. 08 14:50:57 vpn.tp2.cesi openvpn[1068]: Fri Jan  8 14:50:57 2021 CESI_CLIENT/10.88.88.1:64736 Incoming Data Channel: Cipher 'AES-256... bit key
Hint: Some lines were ellipsized, use -l to show in full.
```
* paramètre -l (full)
```bash=
$ sudo systemctl status  openvpn@cesi_vpn -l
```
```bash
● openvpn@cesi_vpn.service - OpenVPN Robust And Highly Flexible Tunneling Application On cesi_vpn
   Loaded: loaded (/usr/lib/systemd/system/openvpn@.service; enabled; vendor preset: disabled)
   Active: active (running) since ven. 2021-01-08 14:22:22 CET; 33min ago
 Main PID: 1068 (openvpn)
   Status: "Initialization Sequence Completed"
   CGroup: /system.slice/system-openvpn.slice/openvpn@cesi_vpn.service
           └─1068 /usr/sbin/openvpn --cd /etc/openvpn/ --config cesi_vpn.conf

janv. 08 14:50:56 vpn.tp2.cesi openvpn[1068]: Fri Jan  8 14:50:56 2021 CESI_CLIENT/10.88.88.1:64736 MULTI_sva: pool returned IPv4=10.8.0.6, IPv6=(Not enabled)
janv. 08 14:50:56 vpn.tp2.cesi openvpn[1068]: Fri Jan  8 14:50:56 2021 CESI_CLIENT/10.88.88.1:64736 MULTI: Learn: 10.8.0.6 -> CESI_CLIENT/10.88.88.1:64736
janv. 08 14:50:56 vpn.tp2.cesi openvpn[1068]: Fri Jan  8 14:50:56 2021 CESI_CLIENT/10.88.88.1:64736 MULTI: primary virtual IP for CESI_CLIENT/10.88.88.1:64736: 10.8.0.6
janv. 08 14:50:57 vpn.tp2.cesi openvpn[1068]: Fri Jan  8 14:50:57 2021 CESI_CLIENT/10.88.88.1:64736 PUSH: Received control message: 'PUSH_REQUEST'
janv. 08 14:50:57 vpn.tp2.cesi openvpn[1068]: Fri Jan  8 14:50:57 2021 CESI_CLIENT/10.88.88.1:64736 SENT CONTROL [CESI_CLIENT]: 'PUSH_REPLY,redirect-gateway def1 bypass-dhcp,dhcp-option DNS 208.67.222.222,dhcp-option DNS 208.67.220.220,route 10.8.0.1,topology net30,ping 10,ping-restart 120,ifconfig 10.8.0.6 10.8.0.5,peer-id 0,cipher AES-256-GCM' (status=1)
janv. 08 14:50:57 vpn.tp2.cesi openvpn[1068]: Fri Jan  8 14:50:57 2021 CESI_CLIENT/10.88.88.1:64736 Data Channel: using negotiated cipher 'AES-256-GCM'
janv. 08 14:50:57 vpn.tp2.cesi openvpn[1068]: Fri Jan  8 14:50:57 2021 CESI_CLIENT/10.88.88.1:64736 Outgoing Data Channel: Cipher 'AES-256-GCM' initialized with 256 bit key
janv. 08 14:50:57 vpn.tp2.cesi openvpn[1068]: Fri Jan  8 14:50:57 2021 CESI_CLIENT/10.88.88.1:64736 Incoming Data Channel: Cipher 'AES-256-GCM' initialized with 256 bit key
janv. 08 14:55:02 vpn.tp2.cesi openvpn[1068]: Fri Jan  8 14:55:02 2021 CESI_CLIENT/10.88.88.1:64736 [CESI_CLIENT] Inactivity timeout (--ping-restart), restarting
janv. 08 14:55:02 vpn.tp2.cesi openvpn[1068]: Fri Jan  8 14:55:02 2021 CESI_CLIENT/10.88.88.1:64736 SIGUSR1[soft,ping-restart] received, client-instance restarting
```

